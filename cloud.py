# coding: utf-8

from leancloud import Engine
from leancloud import LeanEngineError
from leancloud import LeanCloudError
from musicdl import musicdl
from concurrent.futures import ThreadPoolExecutor
import requests
import json
import leancloud

leancloud.init("F6FdBeOJP45jnBHoPveBrAte-gzGzoHsz", "X0NQavtk1GqLBa5b5NwXA8fc")

executor = ThreadPoolExecutor(10)
config = {'logfilepath': 'musicdl.log', 'savedir': 'downloaded',
          'search_size_per_source': 5, 'proxies': {}}
target_srcs = [
    'kugou', 'netease', 'migu', 'joox', 'yiting',
]
client = musicdl.musicdl(config=config)
engine = Engine()


@engine.define
def hello(**params):
    session = engine.current.session_token
    url = "https://f6fdbeoj.lc-cn-n1-shared.com/1.1/classes/test"

    payload = json.dumps({'name':'kuangl'})
    headers = {
        'X-LC-Id': 'F6FdBeOJP45jnBHoPveBrAte-gzGzoHsz',
        'X-LC-Key': 'X0NQavtk1GqLBa5b5NwXA8fc',
        'X-LC-Session': session,
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    if 'name' in params:
        return 'Hello, {}!'.format(params['name'])
    else:
        return 'Hello, LeanCloud!'


@engine.define
def search_music(**params):
    session = engine.current.session_token
    try:
        # 使用session登录用户
        leancloud.User.become(session)
    except LeanCloudError as e:
        return 'session异常'
    if 'music_name' in params:
        if 'singer_name' in params:
            executor.submit(get_download_url, session,
                            params['music_name'], params['singer_name'])
        else:
            executor.submit(get_download_url, session, params['music_name'])
        return '开始搜索..  {}!'.format(params['music_name'])
    else:
        return '参数异常'


def get_download_url(session, music_name, artist_name=''):
    search_results = client.search(music_name, target_srcs)
    music_name = music_name.lower()
    artist_name = artist_name.lower()
    results = []
    for r in target_srcs:
        if r in search_results.keys():
            for data in search_results[r]:
                if True if artist_name == '' else data['singers'].lower() == artist_name \
                                                  and data['songname'].lower() == music_name:
                    results.append({'music_name': music_name, 'artist_name': artist_name,
                                    'download_url': data['download_url'], 'lyric': data['lyric'],
                                    'filesize': data['filesize'], 'ext': data['ext']})

                    save_data({"music_name": music_name, "artist_name": data['singers'], "album": data['album'],
                               "download_url": data['download_url'], "lyric": data['lyric'], "filesize":
                               data['filesize'], "ext": data['ext']}, session)
        else:
            continue
    return results


def save_data(data, session):
    url = "https://f6fdbeoj.lc-cn-n1-shared.com/1.1/classes/music_tmp"
    payload = json.dumps(data)
    headers = {
        'X-LC-Id': 'F6FdBeOJP45jnBHoPveBrAte-gzGzoHsz',
        'X-LC-Key': 'X0NQavtk1GqLBa5b5NwXA8fc',
        'X-LC-Session': session,
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)


